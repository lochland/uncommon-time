"""
    Returns a set of possible beat configurations given a number of beats
"""

#!/usr/bin/env python
# coding: utf-8

from itertools import chain, combinations

def unit(length):
    """ Define set of possible beats, where 0 and 1 are weak and strong beats,
        respectively """
    return [0, 1] * length

def repeats(tup):
    """ Checks for illegal repeats: a given unit must be broken down into
        groups of twos and threes """
    for i in range(len(tup) - 1):
        if tup[i:i+3] == (0, 0, 0) or tup[i:i+2] == (1, 1):
            return True
    return False

def all_beats(unit):
    """ Returns every possible permutation of strong and weak beats """
    return chain(*map(lambda x: combinations(unit, x), range(0, len(unit)+1)))

def beat_filter(number_beats):
    """ Returns a set of beat configurations that are allowable
        Rules are: first beat must be strong, second beat must be weak,
        last beat must be strong, and there may be only two weak beats
        or one strong beat consecutively. """
    beats = set()
    for beat in all_beats(unit(number_beats)):
        if len(beat) == len(unit(number_beats)) // 2 \
        and beat[0] == 1 \
        and beat[1] == 0 \
        and beat[-1] == 0 \
        and not repeats(beat):
            beats.add(beat)
    return beats

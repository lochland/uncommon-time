# Uncommon time

Find all possible combinations of subdivisions given a number of beats per
bar.

Live at [https://lochland.gitlab.io/uncommon-time/](https://lochland.gitlab.io/uncommon-time/).

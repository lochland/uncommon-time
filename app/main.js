//  Uncommon time 1.0
//  https://lochland.gitlab.io/uncommon-time
//  (c) 2017 Lochlan Morrissey
//  Uncommon time may be freely distributed under the MIT license.

// generates a 'superposition' of bars of length l that adheres to
// interpolated rules
function barOfLength(l) {
  var bar = Array();
  for (var i = 0; i < l - 1; ++i) {
    // (1) first beat of bar must be strong
    if (i == 0){
      bar.push([1]);
    // (2) second beat of bar must be weak
    // (3) final beat of bar must be weak
    } if (i == 0 || i == l - 2) {
      bar.push([0]);
    // beat may be weak or strong otherwise
    } else {
      bar.push([1, 0]);
    }
  }
  return bar;
}

// defines set of rules to check for illegal sequences: may only be at most
// two consecutive weak beats, and one consecutive strong beat
function repeats(seq) {
  for (var i = 0; i < seq.length; ++i) {
    var z = seq.slice(i, i + 3);
    var o = seq.slice(i, i + 2);
    if (_.isEqual(z, [0, 0, 0])) {
      return false
    }
    if (_.isEqual(o, [1, 1])) {
      return false }
  }
  return true;
}

// returns all possible bars of length n that adhere to the aforementioned
// rules
function legalBeats(n) {
  return product(barOfLength(n)).filter((v, i, a) => a.indexOf(v) === i).filter(repeats);
};

// makes stave using VexFlow
function makeStave(nr, groove) {
  VF = Vex.Flow;

  // output to output div
  var div = document.getElementById("output")
  var renderer = new VF.Renderer(div, VF.Renderer.Backends.SVG);

  // set stave formatting
  renderer.resize(500, 150);
  var context = renderer.getContext();
  context.setFont("Arial", 10, "").setBackgroundFillStyle("#eed");
  var stave = new VF.Stave(10, 40, 450);
  // set stave to percussion clef with nr beats, where nr is the bar length set below
  stave.addClef("percussion").addTimeSignature(nr+"/8");
  // draw the stave
	stave.setContext(context).draw();

  // generate an array of nr length, which sets nr notes to b
	var notes = [];
  for (var i = 0; i < nr; ++i) {
		notes.push(new VF.StaveNote({clef: "percussion", keys: ["b/4"], duration: "8" }));
  };

  // return an array of threes and twos that describes the subdivision,
  // based on consecutive zeroes in the array;
  // does so by joining array into a string, reconstituting an array by
  // splitting it by strong beats (ones), removing the first element of
  // the array, and finding the length of the remaining
  // elements of the array, which is the number of consecutive zeroes.
  // Adds one to the this number of zeroes (since all weak beats are preceded
  // by a strong beat.
	var chunks = [], grooveDecomposed = groove.join('').split(1).slice(1);
  for (var i = 0; i < grooveDecomposed.length; ++i) {
	  chunks.push(grooveDecomposed[i].length + 1);
	};

  // uses the array returned by the previous function to return an array VexFlow
  // fraction objects to be used in the automated beaming
	var subdivision = [];
	for (var i = 0; i < chunks.length; i++) {
		subdivision.push(new Vex.Flow.Fraction(chunks[i], 8));
	};

  // generates beams of the notes
	var beams = VF.Beam.generateBeams(notes, {
		stem_direction: -1,
		groups: subdivision,
	});

  // draw notes
	Vex.Flow.Formatter.FormatAndDraw(context, stave, notes);
	beams.forEach(function(b) {b.setContext(context).draw()})
}

// define function to return number of beats from form
window.onload = function getNr() {
  var el = document.getElementById("nrBeats");
  el.onkeypress = function(e) {
    if (e.which == 13) {
      // clear output div
      document.getElementById("output").innerHTML = "";
      var nr = el.value;
      // checks if nr is (1) a number; (2) an integer; (3) below 17;
      // (4) above 4
      if (!(isNaN(nr)) && Math.floor(nr) == nr && nr < 17 && nr > 4) {
        var beats = legalBeats(nr);
        for (var i = 0; i < beats.length; ++i) {
          makeStave(nr, beats[i]);
        }
      } else {
        document.getElementById("output").innerHTML += '<div id="alert">Please enter an integer between 5 and 15.</div>'
      };
    };
  };
};
